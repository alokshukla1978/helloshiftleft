#!/usr/bin/python
# pip install requests
import os
import sys

import requests

# Mandatory Repository Variables
SHIFTLEFT_ORG_ID = os.getenv("SHIFTLEFT_ORG_ID")
SHIFTLEFT_ACCESS_TOKEN = os.getenv("SHIFTLEFT_ACCESS_TOKEN")
BITBUCKET_TOKEN = os.getenv("BITBUCKET_TOKEN")

# Collect the required variables
BRANCH = os.getenv("BITBUCKET_BRANCH", "")
BITBUCKET_REPO_FULL_NAME = os.getenv("BITBUCKET_REPO_FULL_NAME")
BITBUCKET_REPO_OWNER = os.getenv("BITBUCKET_REPO_OWNER")
BITBUCKET_WORKSPACE = os.getenv("BITBUCKET_WORKSPACE")
BITBUCKET_REPO_SLUG = os.getenv("BITBUCKET_REPO_SLUG")
BITBUCKET_COMMIT = os.getenv("BITBUCKET_COMMIT")
BITBUCKET_PR_ID = os.getenv("BITBUCKET_PR_ID")
BITBUCKET_PR_DESTINATION_BRANCH = os.getenv("BITBUCKET_PR_DESTINATION_BRANCH")

if not BITBUCKET_REPO_SLUG or not SHIFTLEFT_ORG_ID:
    print(
        "Run this script from a Bitbucket pipeline or set the required environment variables"
    )
    sys.exit(1)
SL_FINDINGS_URL = (
    f"https://www.shiftleft.io/api/v4/orgs/{SHIFTLEFT_ORG_ID}/apps/{BITBUCKET_REPO_SLUG}/findings"
)
BITBUCKET_URL = f"http://api.bitbucket.org/2.0/repositories/{BITBUCKET_REPO_FULL_NAME}/commit/{BITBUCKET_COMMIT}/reports/shiftleft-ngsast"

BITBUCKET_PR_URL = f"https://api.bitbucket.org/2.0/repositories/{BITBUCKET_REPO_FULL_NAME}/pullrequests/{BITBUCKET_PR_ID}/comments"

# Use local bitbucket proxy to avoid the need for app password
proxies = {
    "http": "http://localhost:29418",
    "https": "http://localhost:29418",
}


def convert_severity(severity):
    """Convert ShiftLeft severity to Bitbucket insights"""
    if severity == "critical":
        return "CRITICAL"
    elif severity == "moderate":
        return "MEDIUM"
    return "LOW"


def create_pr_comment(counts, total_count, link):
    counts_table = "| Severity | Count |\n"
    counts_table += "| -------- | ----- |\n"
    print (counts, total_count)
    for cnt in counts:
        if cnt.get("key") == "language":
            counts_table = f'{counts_table}| {cnt.get("severity", "").capitalize()} | {cnt.get("count")} |\n'

    body = f"""## ShiftLeft NextGen Analysis Findings

This PR contains {total_count} security findings

{counts_table}

Visit {link} for details.
    """
    print (body)
    if BITBUCKET_TOKEN and BITBUCKET_WORKSPACE:
        rc = requests.post(
            BITBUCKET_PR_URL,
            auth=(
                BITBUCKET_WORKSPACE,
                BITBUCKET_TOKEN,
            ),
            headers={"Content-Type": "application/json"},
            json={"content": {"raw": body}},
        )
        if not rc.ok:
            print (rc.status_code, rc.json())
    else:
        print ("To add PR comments, pass an app password as BITBUCKET_TOKEN variable")


def get_findings():
    """Method to get ShiftLeft findings using v4 api"""
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + SHIFTLEFT_ACCESS_TOKEN,
    }
    # Get all the findings
    # print (SL_FINDINGS_URL)
    r = requests.get(SL_FINDINGS_URL, headers=headers)
    # print (r.status_code, r.json())
    if r.status_code == 200:
        raw_response = r.json()
        if raw_response and raw_response.get("response"):
            response = raw_response.get("response")
            total_count = response.get("total_count")
            scan = response.get("scan")
            scan_id = scan.get("id")
            spid = scan.get("internal_id")
            projectSpId = f'sl/{SHIFTLEFT_ORG_ID}/{scan.get("app")}'
            findings = response.get("findings")
            counts = response.get("counts")
            link = f"https://www.shiftleft.io/findingsSummary/{BITBUCKET_REPO_SLUG}?apps={BITBUCKET_REPO_SLUG}&isApp=1"
            create_pr_comment(counts, total_count, link)
            data_list = [
                {
                    "title": "Safe to merge?",
                    "type": "BOOLEAN",
                    "value": total_count == 0,
                },
            ]
            # Create a PR report based on the total findings
            rr = requests.put(
                f"{BITBUCKET_URL}{scan_id}",
                proxies=proxies,
                headers={"Content-Type": "application/json",},
                json={
                    "title": "ShiftLeft NG SAST",
                    "details": f"This pull request contains {total_count} issues",
                    "report_type": "SECURITY",
                    "reporter": f"ShiftLeft NextGen Analysis for {BITBUCKET_REPO_SLUG}",
                    "link": link,
                    "remote_link_enabled": True,
                    "logo_url": "https://www.shiftleft.io/static/images/ShiftLeft_logo_white.svg",
                    "result": "FAILED" if total_count else "PASSED",
                    "data": data_list,
                },
            )
            if rr.ok:
                # print(rr.status_code, rr.json())
                # For each finding create an annotation
                # TODO: Enhance the script to pass file and line information based v4 flows api
                for f in findings:
                    fid = f.get("id")
                    annotation_url = f"{BITBUCKET_URL}{scan_id}/annotations/ngsast{fid}"
                    finternal = f.get("internal_id")
                    tmpA = finternal.split("/")
                    title = tmpA[0]
                    occurrenceHash = tmpA[-1]
                    annotation = {
                        "title": title,
                        "annotation_type": "VULNERABILITY",
                        "summary": f.get("title"),
                        "details": f.get("description"),
                        "severity": convert_severity(f.get("severity")),
                        "created_on": f.get("created_at"),
                    }
                    ar = requests.put(
                        annotation_url,
                        proxies=proxies,
                        headers={"Content-Type": "application/json",},
                        json=annotation,
                    )
                    if not ar.ok:
                        break
                    # print(ar.status_code, ar.json())


if __name__ == "__main__":
    get_findings()
